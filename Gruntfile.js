module.exports = function(grunt) {
  grunt.initConfig({
    less: {
      default: {
        files: {
          "src/css/style.css": "src/less/style.less"
        }
      }
    },

    'http-server': {
      'dev': {
        root: 'src',
        port: 4242,    
        host: "0.0.0.0",
        cache: 0,
        showDir : true,
        autoIndex: true,
        ext: 'html',
        runInBackground: false
      }
    },

    watch: {
      files: ['**/*.less'],
      tasks: ['less']
    },

    concurrent: {
      dev: {
        tasks: ['watch', 'http-server:dev']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-http-server');
  grunt.loadNpmTasks('grunt-concurrent');

  grunt.registerTask('dev', ['concurrent:dev']);
};