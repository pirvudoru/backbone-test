### Dependencies ###
* npm
* bower
* grunt-cli

### Setup ###
````
npm install
bower install
````

### Run ###
````
grunt dev
````

### [Use](http://localhost:4242) ###