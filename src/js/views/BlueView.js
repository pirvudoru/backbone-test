define(['views/BaseView'], function (BaseView) {
  return BaseView.extend({
    templateName: 'blue-view-template'
  })
});