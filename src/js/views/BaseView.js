define(['backbone', 'underscore', 'jquery'], function (Backbone, _, $) {
  return Backbone.View.extend({
    subViews: [],

    render: function () {
      var templateName = _.result(this, 'templateName');
      var template = _.template($('#' + templateName).html());

      this.$el.html(template(this.getRenderContext()));

      _.each(this.subViews, function (subViewDefinition) {
        var selector = subViewDefinition[0],
          subViewKey = subViewDefinition[1],
          subView = this[subViewKey],
          $el = this.$(selector);

        if (!subView || !$el.length) return;

        $el.append(subView.$el);
        subView.render();
      }, this);

      return this;
    },

    getRenderContext: function () {
      return {
        view: this,
        model: this.model
      };
    }
  })
});