define(['views/BaseView'], function (BaseView) {
  return BaseView.extend({
    templateName: 'time-view-template'
  })
});