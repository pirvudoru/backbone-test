define(['views/BaseView'], function (BaseView) {
  return BaseView.extend({
    templateName: 'yellow-view-template'
  })
});