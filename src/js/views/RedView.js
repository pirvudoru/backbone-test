define(['views/BaseView', 'views/YellowView', 'views/BlueView'], function (BaseView, YellowView, BlueView) {
  return BaseView.extend({
    templateName: 'red-view-template',

    subViews: [
      ['.red-view-inner-color', 'innerView']
    ],

    initialize: function (options) {
      BaseView.prototype.initialize.apply(this, arguments);

      options = options || {};
      var typeMap = {
        'yellow': YellowView,
        'blue': BlueView
      };

      var nestedViewType = typeMap[options.innerColor];
      if (nestedViewType) {
        this.innerView = new nestedViewType();
      }
    }
  });
});