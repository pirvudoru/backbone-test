define(['backbone', 'moment'], function (Backbone, moment) {
  return Backbone.Model.extend({
    url: function () {
      return "https://script.googleusercontent.com/macros/echo?user_content_key=Y0XXXrylXDDbYONC1GngFb8MRofnSXO9hc-By4PTTZstdXKR63JwraTJQ_3MF7R95OckSmgQ9-ZN1m8j-Xmj9kr92Gma4Cssm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnJ9GRkcRevgjTvo8Dc32iw_BLJPcPfRdVKhJT5HNzQuXEeN3QFwl2n0M6ZmO-h7C6eIqWsDnSrEd&lib=MwxUjRcLr2qLlnVOLh12wSNkqcO1Ikdrk";
    },

    toString: function () {
      return moment(this.get('fulldate')).toString();
    }
  })
});