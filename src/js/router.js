define(['underscore', 'backbone', 'jquery', 'models/TimeInfo', 'views/TimeView', 'views/RedView'],
  function (_, Backbone, $, TimeInfo, TimeView, RedView) {
    return Backbone.Router.extend({
      routes: {
        'time': 'time',
        'colors(/:type)': 'colors',
        '*catchAll': 'catchAll'
      },

      initialize: function () {
        this.$mainContent = $('#main-content');

        Backbone.Router.prototype.initialize.apply(this, arguments);
      },

      colors: function (type) {
        if (_.isEmpty(type)) {
          this.navigate('colors/yellow', {trigger: true});
          return;
        }

        var view = new RedView({innerColor: type});
        this.renderPage(view);
      },

      time: function () {
        var timeInfo = new TimeInfo();
        timeInfo.fetch()
          .then(_.bind(function () {
            this.renderPage(new TimeView({model: timeInfo}));
          }, this));
      },

      catchAll: function () {
        this.$mainContent.html('404');
      },

      renderPage: function (view) {
        this.$mainContent.empty().append(view.$el);
        view.render();
      }
    });
  });