requirejs.config({
  baseUrl: 'bower_components',
  paths: {
    jquery: 'jquery/dist/jquery',
    backbone: 'backbone/backbone',
    lodash: 'lodash/dist/lodash',
    underscore: 'lodash/dist/lodash',
    moment: 'momentjs/moment',
    models: '../js/models/',
    views: '../js/views/',
    AppRouter: '../js/router'
  }
});

requirejs(['jquery', 'backbone', 'AppRouter'], function   ($, Backbone, AppRouter) {
  var router = new AppRouter();
  Backbone.history.start();
});